import requests
from bs4 import BeautifulSoup
import mysql.connector as mysql
import urllib.request
import concurrent.futures
import time
from datetime import datetime
import json

start_time = time.time()
print("crawl dev")
print("Bắt đầu chạy: " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
 # kết nối database
db = mysql.connect(
    host = "45.77.173.173",
    user = "root",
    passwd = "tovi",
    database = "VersionCode"
)
cursor = db.cursor()

results = []
def crawl_discover(page):
    url = "https://apkpure.com/discover?page=" + str(page)
    response = requests.get(url)
    if response.status_code == 200: 
        soup = BeautifulSoup(response.content, "html.parser")
        a_list = soup.select("body > div.main > div> div> div.editors_m_2 > h3 > a") 
        for i in a_list:
            appid = i.attrs["href"].split("/")[2]
            inserted_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            source = "apkpure.com"
            results.append([appid,inserted_time,source])
count_insert=0
def insert_database(value):
    query = "INSERT IGNORE INTO appStore_0709(appid, inserted_time, source) VALUES (%s,%s,%s)"
    values = value
    cursor.execute(query, values)  
    db.commit()
    # print(cursor.rowcount, "record inserted")
    if cursor.rowcount >0:
       global count_insert 
       count_insert= count_insert+1  
print("crawling...")
list_page = [x for x in range(497)]
chunks = [list_page[x:x+25] for x in range(0, len(list_page), 25)]

for chunk in chunks:
    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(crawl_discover, chunk)
time2= (time.time() - start_time)/60 

 

# INSERT DATA
print("inserting...")
for value in results:
    insert_database(value)
# print(results)

# with open('appid.txt', 'w') as filehandle:
#     json.dump(results,filehandle)
time3= (time.time() - start_time)/60

print("Số lượng bản ghi %s" %count_insert)

print("--- Thời gian crawl %s minutes ---" % time2)
print("--- Thời gian insert %s minutes ---" %(time3-time2))
print("--- Thời gian chạy xong %s minutes ---" %time3)
