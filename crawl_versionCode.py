import requests
from bs4 import BeautifulSoup
import mysql.connector as mysql
import urllib.request
import urllib.error
import urllib.parse
import time
start_time = time.time()

 # kết nối database
db = mysql.connect(
    host = "128.199.160.174",
    user = "root",
    passwd = "tovi",
    database = "VersionCode"
)
cursor = db.cursor()

#Lấy các appid
query = "SELECT `appid` FROM `appStore2` WHERE `id` BETWEEN 100000 AND 110000"
cursor.execute(query)
appids = cursor.fetchall()
print(len(appids))
print("--- %s seconds ---" % (time.time() - start_time))


# Xử lý các versions có variant
def crawl_variant(href):
    # Crawl html
    url = "https://apkpure.com" + href
    appid = href.split("/")[2]
    response = requests.get(url)
    
    soup = BeautifulSoup(response.content, "html.parser")

    # Xử lý dữ liệu
    
    # Tìm các thẻ chứa VersionCode không có variant (Các thẻ div có class ver-info)
    text_variants = soup.findAll('div', class_='ver-info')
    text_variants_convert_str = []
    for x in text_variants:
        text_variants_convert_str.append(str(x))

  
    for x in text_variants_convert_str:
        # lấy phần versioncode
        versionCode = x.split(")</div>")[0].split("(")[-1]
        # lấy phần versionstring
        versionString = x.split("</strong> ")[1].split(" (")[0]
        #lấy phần uploaddate
        uploadDate = x.split("</p>")[0].split("</strong>")[-1]
        
    
        # #Thêm vào csdl vString và vCode
        query = "INSERT IGNORE INTO testVersionCode(appid, versionCode, versionString,uploadDate) VALUES (%s,%s,%s,%s)"
        values = (appid,versionCode,versionString,uploadDate)
        cursor.execute(query, values)  
        db.commit()
        print(cursor.rowcount, "record inserted")
   

# Xử lý các version không có variant
def crawl(appid):
    # Crawl html
    url = "https://apkpure.com/a/" + appid + "/versions"
    response = requests.get(url)
    if response.status_code != 404:
        soup = BeautifulSoup(response.content, "html.parser")

        # Xử lý dữ liệu
        # Tìm các thẻ chứa VersionCode không có variant (Các thẻ div có class ver-info-top)
        texts_no_variant = soup.findAll('div', class_='ver-info')

        # Convert các thẻ tìm được sang str
        text_variants_convert_str = []
        for x in texts_no_variant:
            text_variants_convert_str.append(str(x))

        # lấy versionCode lưu vào list versionCodes, versionString lưu vào list versionStrings
        for x in text_variants_convert_str:
            # lấy phần tử đầu tiên ở giữa 2 dấu ()
            versionCode = x.split(")</div>")[0].split("(")[-1]
            # lấy phần tử đầu tiên ở giữa thẻ </strong> và (
            versionString = x.split("</strong> ")[1].split(" (")[0]
            #lấy phần tử đầu tiên giữa thẻ /p và /strong
            uploadDate = x.split("</p>")[0].split("</strong>")[-1]
            

            #Thêm vào csdl vString và vCode
            query = "INSERT IGNORE INTO testVersionCode(appid, versionCode, versionString,uploadDate) VALUES (%s,%s,%s,%s)"
            values = (appid,versionCode,versionString,uploadDate)
            cursor.execute(query, values)  
            db.commit()
            print(cursor.rowcount, "record inserted")
        # Lấy versionString của loại có variant
        text_variants=soup.findAll('a')

        for x in text_variants:
            #lấy link của tùng bản variant
            if "variants" in str(x):
                href = x.get("href")
                crawl_variant(href)


for appid in appids:
    #chuyển appid về string
    appid = str(appid).split("'")[1].split("'")[-1]
    crawl(appid)

time= (time.time() - start_time)/60
print("--- %s minutes ---" % time)