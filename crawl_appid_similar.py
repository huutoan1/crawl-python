import requests
from bs4 import BeautifulSoup
import mysql.connector as mysql
import urllib.request
import concurrent.futures
import time
from datetime import datetime
import json

start_time = time.time()
print("crawl similar")
print("Bắt đầu chạy: " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
 # kết nối database
db = mysql.connect(
    host = "45.77.173.173",
    user = "root",
    passwd = "tovi",
    database = "VersionCode"
)
cursor = db.cursor()

#LẤY APPID TỪ DATABASE(đang lấy đến 300k)
query = "SELECT `appid` FROM `appStore_0709` WHERE `id` BETWEEN 1000001 AND 1200000"
cursor.execute(query)
appids = cursor.fetchall()
appid_list =[str(appid).split("'")[1].split("'")[-1] for appid in appids]
print("số lượng appid " + str(len(appids)))
time1 = time.time() - start_time
print("--- Time to lấy xong appid %s seconds ---" %time1)
# LIST CHỨA KẾT QUẢ
results = []
# Crawl appid funtion
def crawl_appid(appid):
    # Get html 
    try:
        url = "https://apkpure.com/similar/"+appid
        response = requests.get(url)
        if response.status_code == 200: 
            soup = BeautifulSoup(response.content, "html.parser")
            # Get href
            list_appid = soup.findAll("dd", class_ = "title-dd")
            for x in list_appid:
                a = x.findChildren()
                href = str(a).split('"')[1].split("/")[2]
                inserted_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                source = "apkpure.com"
                results.append([href,inserted_time,source]) 
    except:
        print("eror: "+ appid)
#INSERT DATABASE
count_insert=0
def insert_database(value):
    query = "INSERT IGNORE INTO appStore_0709(appid, inserted_time, source) VALUES (%s,%s,%s)"
    values = value
    cursor.execute(query, values)  
    db.commit()
    # print(cursor.rowcount, "record inserted")
    if cursor.rowcount >0:
       global count_insert 
       count_insert= count_insert+1     

# CHIA APPID_LIST THÀNH CÁC LIST CON CÓ 25 PHẦN TỬ



# CRAWL DATA
# crawl_appid("com.garena.game.kgvn")
print("crawling...")

with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(crawl_appid, appid_list)

time2= (time.time() - start_time)/60 
print("--- Time to crawl %s minutes ---" % time2)
# print(results)
# INSERT DATA
print("inserting...")
# for value in results:
#     insert_database(value)
with open('value_similar.txt', 'w') as filehandle:
    json.dump(results,filehandle)
time3= (time.time() - start_time)/60
print("1M-1,2M")
# print("Số lượng bản ghi %s" %count_insert)
print("--- Time to get appid %s seconds ---" %time1)
print("--- Time to crawl %s minutes ---" % time2)
print("--- Time to insert %s minutes ---" %(time3-time2))
print("--- Time to run %s minutes ---" %time3)