import requests
from bs4 import BeautifulSoup
import mysql.connector as mysql
import urllib.request
import concurrent.futures
import time
from datetime import datetime
import json

start_time = time.time()
print("crawl dev")
print("Bắt đầu chạy: " + datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
 # kết nối database
db = mysql.connect(
    host = "45.77.173.173",
    user = "root",
    passwd = "tovi",
    database = "VersionCode"
)
cursor = db.cursor()

#LẤY APPID TỪ DATABASE
query = "SELECT `appid` FROM `appStore_0709` WHERE `id` BETWEEN 600001 AND 800000"
cursor.execute(query)
appids = cursor.fetchall()
appid_list =[str(appid).split("'")[1].split("'")[-1] for appid in appids]
print(len(appids))
time1 = time.time() - start_time
print("--- Thời gian lấy xong appid %s seconds ---" %time1)
# LIST CHỨA KẾT QUẢ
results = []
href = []
# Crawl appid funtion
def crawl_href_dev(appid):
    # Get html 
    try:
        url = "https://apkpure.com/huudz/"+appid
        response = requests.get(url)
        if response.status_code == 200: 
            soup = BeautifulSoup(response.content, "html.parser")
            # Get href
            a_list = soup.select("body > div.main.page-q > div.left > div:nth-child(8) > div.title > div.more > a") 
            for i in a_list:
                # print(i.attrs["href"])
                crawl_appid(i.attrs["href"])
    except:
        print("eror")
#CRAWL APPID
def crawl_appid(href):
    try:
        for x in range(1,10):
            print(x)
            url = "https://apkpure.com"+href+"?page="+str(x)
            response = requests.get(url)
            if response.status_code == 200: 
                soup = BeautifulSoup(response.content, "html.parser") 
                a_list = soup.select("body > div.main > div.left > div > div > dl > dd > p.search-title > a")
                for i in a_list:
                    appid = i.attrs["href"].split("/")[2]
                    inserted_time = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
                    source = "apkpure.com"
                    results.append([appid,inserted_time,source])
            else:
                break
    except:
        print("error")
#INSERT DATABASE
count_insert=0
def insert_database(value):
    query = "INSERT IGNORE INTO appStore_0709(appid, inserted_time, source) VALUES (%s,%s,%s)"
    values = value
    cursor.execute(query, values)  
    db.commit()
    # print(cursor.rowcount, "record inserted")
    if cursor.rowcount >0:
       global count_insert 
       count_insert= count_insert+1     



# CRAWL DATA
# crawl_href_dev("com.google.android.apps.tachyon")

print("crawling...")

with concurrent.futures.ThreadPoolExecutor() as executor:
    executor.map(crawl_href_dev, appid_list)
time2= (time.time() - start_time)/60 

 

# INSERT DATA
print("inserting...")
# for value in results:
#     insert_database(value)
print(len(results))

with open('value_dev.txt', 'w') as filehandle:
    json.dump(results,filehandle)


time3= (time.time() - start_time)/60
print("600k-800K")
print("Số lượng bản ghi %s" %count_insert)
print("--- Thời gian lấy xong appid %s seconds ---" %time1)
print("--- Thời gian crawl %s minutes ---" % time2)
print("--- Thời gian insert %s minutes ---" %(time3-time2))
print("--- Thời gian chạy xong %s minutes ---" %time3)